package main

import (
	"fmt"
	"time"
	"utils"

	"os/exec"
)

func main() {
	p := fmt.Println

	out, err := exec.Command("uuidgen").Output()
	if err != nil {
		p(err)
	}
	p("%s", out)

	ID := utils.GenerateId(10)
	p("ID :: ", ID)

	timeFormat := utils.FormatTimeForLog(time.Now())
	p("timeFormat :: ", timeFormat)

	MG5Hash := utils.GetMD5Hash(`Test`)
	p("MG5Hash :: ", MG5Hash)

	var str = `Test`
	var str_array = []string{`Yellow`, `Cat`, `Dog`, `TEST`, `Play`}

	if utils.StringInSlice(str, str_array) {
		p("String", str, "is in array", str_array)
	} else {
		p("Array", str_array, "hasn`t string", str)
	}
}
