package utils

import (
	"crypto/md5"
	"fmt"
	"math/rand"
	"strings"
	"time"
)

// Формат для вывода лога
const LOG_TIME_FORMAT = `2006-01-02T15:04:05.999999999`
const CHARS = `abcdef0123456789`

func init() {
	rand.Seed(time.Now().UnixNano())
}

func GenerateId(strlen int) string {
	result := make([]byte, strlen)
	for i := 0; i < strlen; i++ {
		result[i] = CHARS[rand.Intn(len(CHARS))]
	}
	return string(result)
}

func FormatTimeForLog(t time.Time) string {
	return t.Format(LOG_TIME_FORMAT)
}

// Генерация md5 хеша
func GetMD5Hash(password string) string {
	hasher := md5.New()
	hasher.Write([]byte(password))
	return fmt.Sprintf(`%x`, hasher.Sum(nil))
}

// Поиск строки в массиве
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if strings.EqualFold(b, a) {
			return true
		}
	}
	return false
}
