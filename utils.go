package utils

import (
	"crypto/md5"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

// Формат для вывода лога
const LOG_TIME_FORMAT = `2006-01-02T15:04:05.000000000`
const CHARS = `abcdef0123456789`

func init() {
	rand.Seed(time.Now().UnixNano())
}

func GenerateId(strlen int) string {
	result := make([]byte, strlen)
	for i := 0; i < strlen; i++ {
		result[i] = CHARS[rand.Intn(len(CHARS))]
	}
	return string(result)
}

func FormatTimeForLog(t time.Time) string {
	return t.Format(LOG_TIME_FORMAT)
}

// Генерация md5 хеша
func GetMD5Hash(password string) string {
	hasher := md5.New()
	hasher.Write([]byte(password))
	return fmt.Sprintf(`%x`, hasher.Sum(nil))
}

// Поиск строки в массиве
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if strings.EqualFold(b, a) {
			return true
		}
	}
	return false
}

// Очитка ассоциированного массива (map)
func ClearMap(map_ map[string]interface{}) {
	for k := range map_ {
		delete(map_, k)
	}
}

// Приведение к типу string
func ToString(i interface{}) (string, error) {
	switch s := i.(type) {
	case string:
		return s, nil
	case bool:
		return strconv.FormatBool(s), nil
	case float64:
		return strconv.FormatFloat(i.(float64), 'f', -1, 64), nil
	case int64:
		return strconv.FormatInt(i.(int64), 10), nil
	case int:
		return strconv.FormatInt(int64(i.(int)), 10), nil
	case []byte:
		return string(s), nil
	case nil:
		return "", nil
	case fmt.Stringer:
		return s.String(), nil
	case error:
		return s.Error(), nil
	default:
		return "", fmt.Errorf("Unable to Cast %#v to string", i)
	}
}
