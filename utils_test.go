package utils

import (
	"fmt"
	"testing"
	"time"
)

func TestGenerateId(t *testing.T) {
	t.Log(GenerateId(10))
	t.Log(GenerateId(20))
}

func TestFormatTimeForLog(t *testing.T) {
	value := `2012-11-01T22:08:41`
	t1, err := time.Parse(time.RFC3339, `2012-11-01T22:08:41+00:00`)
	check(t, err)
	result := FormatTimeForLog(t1)
	assert(t, result == value, fmt.Sprintf(`Time log format looked for %s, and received %s `, value, result))
}

func TestGetMD5Hash(t *testing.T) {
	value := `0cbc6611f5540bd0809a388dc95a615b`
	result := GetMD5Hash(`Test`)
	assert(t, result == value, fmt.Sprintf(`Md5 hash looked for %s, and received %s `, value, result))
}

func TestStringInSlice(t *testing.T) {
	var str = `Test1`
	var str_array = []string{`Yellow`, `Cat`, `Dog`, `TEST`, `Play`}
	result := StringInSlice(str, str_array)
	assert(t, result, fmt.Sprintf(`String "Test" is in array`))

	str = `Test`
	result = StringInSlice(str, str_array)
	assert(t, !result, fmt.Sprintf(`String "Test" is not contained in an array`))
}

func check(t *testing.T, e error) {
	if e != nil {
		t.Error(e)
	}
}

func assert(t *testing.T, condition bool, assertion string) {
	if !condition {
		t.Errorf("Assertion failed: %v", assertion)
	}
}
